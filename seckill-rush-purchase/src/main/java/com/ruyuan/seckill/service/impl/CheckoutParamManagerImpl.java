package com.ruyuan.seckill.service.impl;


import com.ruyuan.seckill.cache.Cache;
import com.ruyuan.seckill.constant.CheckoutParamName;
import com.ruyuan.seckill.domain.MemberAddress;
import com.ruyuan.seckill.domain.ReceiptHistory;
import com.ruyuan.seckill.domain.enums.CachePrefix;
import com.ruyuan.seckill.domain.enums.PaymentTypeEnum;
import com.ruyuan.seckill.domain.vo.CheckoutParamVO;
import com.ruyuan.seckill.service.CheckoutParamManager;
import com.ruyuan.seckill.service.MemberAddressClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 结算参数 业务层实现类
 */
@Service
public class CheckoutParamManagerImpl implements CheckoutParamManager {

    @Autowired
    private Cache cache;

    @Autowired
    private MemberAddressClient memberAddressClient;


    @Override
    public CheckoutParamVO getParam(Integer uid) {
        CheckoutParamVO  param = new CheckoutParamVO();
        MemberAddress address = this.memberAddressClient.getDefaultAddress(uid);
        if ( address == null) {
            address = new MemberAddress();
            address.setAddrId(uid);
            String memberAddressKey = uid + "::memberAddress";
            cache.put(memberAddressKey, address);
        }
        int addrId = address.getAddrId();
        // 默认配送地址
        param.setAddressId(addrId);
        // 默认支付方式
        param.setPaymentType(PaymentTypeEnum.defaultType());
        // 默认不需要发票
        ReceiptHistory receipt = new ReceiptHistory();
        param.setReceipt(receipt);
        // 收获时间
        param.setReceiveTime("任意时间");
        this.write(param, uid);
        return param;
    }

    @Override
    public void setClientType(String clientType, Integer uid) {
        this.cache.putHash(getRedisKey(uid), CheckoutParamName.CLIENT_TYPE, clientType);
    }

    @Override
    public void checkCod(PaymentTypeEnum paymentTypeEnum, Integer addressId) {
        if (!PaymentTypeEnum.COD.equals(paymentTypeEnum)) {
            return;
        }
    }


    /**
     * 读取Key
     *
     * @return
     */
    private String getRedisKey(Integer uid) {

        return CachePrefix.CHECKOUT_PARAM_ID_PREFIX.getPrefix() + uid;
    }

    /**
     * 写入map值
     *
     * @param paramVO
     */
    private void write(CheckoutParamVO paramVO, Integer uid) {
        String redisKey = getRedisKey(uid);
        Map<String, Object> map = new HashMap<>(4);

        if (paramVO.getAddressId() != null) {
            map.put(CheckoutParamName.ADDRESS_ID, paramVO.getAddressId());
        }

        if (paramVO.getReceiveTime() != null) {
            map.put(CheckoutParamName.RECEIVE_TIME, paramVO.getReceiveTime());
        }

        if (paramVO.getPaymentType() != null) {
            map.put(CheckoutParamName.PAYMENT_TYPE, paramVO.getPaymentType());
        }
        if (paramVO.getReceipt() != null) {
            map.put(CheckoutParamName.RECEIPT, paramVO.getReceipt());
        }
        if (paramVO.getRemark() != null) {
            map.put(CheckoutParamName.REMARK, paramVO.getRemark());
        }
        if (paramVO.getClientType() != null) {
            map.put(CheckoutParamName.CLIENT_TYPE, paramVO.getClientType());
        }

        this.cache.putAllHash(redisKey, map);
    }


    /**
     * 由Redis中读取出参数
     */
    private CheckoutParamVO read(Integer uid) {
        String key = getRedisKey(uid);
        Map<String, Object> map = this.cache.getHash(key);

        //如果还没有存过则返回null
        if (map == null || map.isEmpty()) {
            return null;
        }

        // 如果取到了，则取出来生成param
        Integer addressId = (Integer) map.get(CheckoutParamName.ADDRESS_ID);
        PaymentTypeEnum paymentType = (PaymentTypeEnum) map.get(CheckoutParamName.PAYMENT_TYPE);
        ReceiptHistory receipt = (ReceiptHistory) map.get(CheckoutParamName.RECEIPT);
        String receiveTime = (String) map.get(CheckoutParamName.RECEIVE_TIME);
        String remark = (String) map.get(CheckoutParamName.REMARK);
        String clientType = (String) map.get(CheckoutParamName.CLIENT_TYPE);


        CheckoutParamVO param = new CheckoutParamVO();

        param.setAddressId(addressId);
        param.setReceipt(receipt);
        if (receiveTime == null) {
            receiveTime = "任意时间";
        }
        param.setReceiveTime(receiveTime);
        param.setRemark(remark);
        if (paymentType == null) {
            paymentType = PaymentTypeEnum.defaultType();
        }

        param.setPaymentType(paymentType);
        param.setClientType(clientType);
        return param;
    }

}
