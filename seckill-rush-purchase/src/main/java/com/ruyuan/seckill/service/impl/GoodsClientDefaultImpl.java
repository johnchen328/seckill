package com.ruyuan.seckill.service.impl;


import com.ruyuan.seckill.domain.vo.CacheGoods;
import com.ruyuan.seckill.domain.vo.GoodsSkuVO;
import com.ruyuan.seckill.service.GoodsClient;
import com.ruyuan.seckill.service.GoodsQueryManager;
import com.ruyuan.seckill.service.GoodsSkuManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 商品对外的接口实现
 */
@Service
public class GoodsClientDefaultImpl implements GoodsClient {


    @Autowired
    private GoodsSkuManager goodsSkuManager;
    @Autowired
    private GoodsQueryManager goodsQueryManager;

    /**
     * 缓存中查询sku信息
     *
     * @param skuId
     * @return
     */
    @Override
    public GoodsSkuVO getSkuFromCache(Integer skuId) {
        return this.goodsSkuManager.getSkuFromCache(skuId);
    }

    /**
     * 缓存中查询商品的信息
     *
     * @param goodsId
     * @return
     */
    @Override
    public CacheGoods getFromCache(Integer goodsId) {
        return this.goodsQueryManager.getFromCache(goodsId);
    }
}
