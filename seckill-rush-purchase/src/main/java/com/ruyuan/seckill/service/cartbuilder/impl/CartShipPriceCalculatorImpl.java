package com.ruyuan.seckill.service.cartbuilder.impl;


import com.ruyuan.seckill.domain.Buyer;
import com.ruyuan.seckill.domain.vo.CartVO;
import com.ruyuan.seckill.service.ShippingManager;
import com.ruyuan.seckill.service.cartbuilder.CartShipPriceCalculator;
import com.ruyuan.seckill.utils.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class CartShipPriceCalculatorImpl implements CartShipPriceCalculator {

    @Autowired
    private ShippingManager shippingManager;

    @Override
    public void countShipPrice(List<CartVO> cartList) {
        shippingManager.setShippingPrice(cartList);
        log.debug("购物车处理运费结果为：{}", JsonUtil.objectToJson(cartList));
    }

    @Override
    public void countShipPrice(List<CartVO> cartList, Buyer buyer) {
        shippingManager.setShippingPrice(cartList,buyer.getUid());
        log.debug("购物车处理运费结果为：{}", JsonUtil.objectToJson(cartList));
    }
}
