package com.ruyuan.seckill.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.io.Serializable;

@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MemberAddress implements Serializable {

    private static final long serialVersionUID = 5386739629590247L;


    /**
     * 主键ID
     */
    private Integer addrId;
    /**
     * 会员ID
     */
    private Integer memberId;
    /**
     * 收货人姓名
     */
    private String name;
    /**
     * 收货人国籍
     */
    private String country;
    /**
     * 所属省份ID
     */
    private Integer provinceId;
    /**
     * 所属城市ID
     */
    private Integer cityId;
    /**
     * 所属县(区)ID
     */
    private Integer countyId;
    /**
     * 所属城镇ID
     */
    private Integer townId;
    /**
     * 所属县(区)名称
     */
    private String county;
    /**
     * 所属城市名称
     */
    private String city;
    /**
     * 所属省份名称
     */
    private String province;
    /**
     * 所属城镇名称
     */
    private String town;
    /**
     * 详细地址
     */
    private String addr;

    /**
     * 联系电话(一般指座机)
     */
    private String tel;
    /**
     * 手机号码
     */
    private String mobile;
    /**
     * 是否为默认收货地址
     */
    private Integer defAddr;
    /**
     * 地址别名
     */
    private String shipAddressName;

    private Region region;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getAddrId() {
        return addrId;
    }

    public void setAddrId(Integer addrId) {
        this.addrId = addrId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getCountyId() {
        return countyId;
    }

    public void setCountyId(Integer countyId) {
        this.countyId = countyId;
    }

    public Integer getTownId() {
        return townId;
    }

    public void setTownId(Integer townId) {
        this.townId = townId;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getDefAddr() {
        return defAddr;
    }

    public void setDefAddr(Integer defAddr) {
        this.defAddr = defAddr;
    }

    public String getShipAddressName() {
        return shipAddressName;
    }

    public void setShipAddressName(String shipAddressName) {
        this.shipAddressName = shipAddressName;
    }

    @JsonIgnore
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    @Override
    public String toString() {
        return "MemberAddress{" +
                "addrId=" + addrId +
                ", memberId=" + memberId +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", provinceId=" + provinceId +
                ", cityId=" + cityId +
                ", countyId=" + countyId +
                ", townId=" + townId +
                ", county='" + county + '\'' +
                ", city='" + city + '\'' +
                ", province='" + province + '\'' +
                ", town='" + town + '\'' +
                ", addr='" + addr + '\'' +
                ", tel='" + tel + '\'' +
                ", mobile='" + mobile + '\'' +
                ", defAddr=" + defAddr +
                ", shipAddressName='" + shipAddressName + '\'' +
                '}';
    }


    /**
     * 获取最低级地区
     *
     * @return
     */
    public Integer actualAddress() {
//        if(this.townId!=null&&townId!=0) {
//            return townId;
//        }
        if (this.countyId != null && countyId != 0) {
            return countyId;
        }
        if (this.cityId != null && cityId != 0) {
            return cityId;
        }
        return provinceId;
    }
}