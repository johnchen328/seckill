package com.ruyuan.seckill.domain.enums;

/**
 * 支付方式
 */
public enum PaymentTypeEnum {

    /**
     * 在线支付
     */
    ONLINE("在线支付"),

    /**
     * 货到付款
     */
    COD("货到付款");

    private String description;


    PaymentTypeEnum(String description){
        this.description = description;
    }

    public static PaymentTypeEnum defaultType(){

        return ONLINE;
    }

    public String description(){
        return this.description;
    }

    public String value(){
        return this.name();
    }

}
