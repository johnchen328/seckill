package com.ruyuan.seckill.domain.enums;

/**
 * 订单类型
 */
public enum OrderTypeEnum {

    /**
     * 普通订单
     */
    NORMAL,

    /**
     * 拼团订单
     */
    PINTUAN,

    /**
     * 换货订单
     */
    CHANGE,

    /**
     * 补发商品订单
     */
    SUPPLY_AGAIN

}
